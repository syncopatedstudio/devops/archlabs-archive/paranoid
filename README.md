# paranoid
A GTK+3 configuration tool for compton forked from [paranoid](https://github.com/semplice/paranoid).
 
# installation 
  
  - copy both paranoid.py and paranoid.glade to /usr/bin/
  
  - copy paranoid.desktop to /usr/share/applications/
